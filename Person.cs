﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crash.FinnishPersonGenerator
{
    public class Person
    {
        public Gender Gender { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string SSN { get; set; }
        public string StreetAddress { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
    }
}
