﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crash.FinnishPersonGenerator
{
    public class PersonOptions
    {
        public Gender? Gender { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
        public string[] EmailDomains { get; set; }
    }
}
