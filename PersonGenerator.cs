﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Crash.FinnishPersonGenerator
{
    public static class PersonGenerator
    {
        private static readonly Random Random = new Random();
        private static readonly List<ValueIndex<string>> FirstNamesMan;
        private static readonly List<ValueIndex<string>> FirstNamesWoman;
        private static readonly List<ValueIndex<string>> MiddleNamesMan;
        private static readonly List<ValueIndex<string>> MiddleNamesWoman;
        private static readonly List<ValueIndex<string>> LastNames;
        private static readonly List<ValueIndex<string>> StreetNames;
        private static readonly List<ValueIndex<(string PostalCode, string City)>> PostalCodes;
        private static readonly Dictionary<int, char> SSNCheckCharacters = new Dictionary<int, char>
        {
            { 0, '0' },
            {1, '1' },
            {2, '2' },
            {3, '3' },
            {4, '4' },
            {5, '5' },
            {6, '6' },
            {7, '7' },
            {8, '8' },
            {9, '9' },
            {10, 'A' },
            {11, 'B' },
            {12, 'C' },
            {13, 'D' },
            {14, 'E' },
            {15, 'F' },
            {16, 'H' },
            {17, 'J' },
            {18, 'K' },
            {19, 'L' },
            {20, 'M' },
            {21, 'N' },
            {22, 'P' },
            {23, 'R' },
            {24, 'S' },
            {25, 'T' },
            {26, 'U' },
            {27, 'V' },
            {28, 'W' },
            {29, 'X' },
            {30, 'Y' }
        };
        private static readonly string[] MobileNumbersPrefixes = new[] { "040", "041", "042", "043", "044", "045", "046", "049", "050" };

        static PersonGenerator()
        {
            var files = typeof(PersonGenerator).Assembly.GetManifestResourceNames();
            var prefix = typeof(PersonGenerator).Assembly.GetName().Name;
            FirstNamesMan = ReadNames($"{prefix}.names_first_man.csv");
            FirstNamesWoman = ReadNames($"{prefix}.names_first_woman.csv");
            MiddleNamesMan = ReadNames($"{prefix}.names_middle_man.csv");
            MiddleNamesWoman = ReadNames($"{prefix}.names_middle_woman.csv");
            LastNames = ReadNames($"{prefix}.names_last.csv");
            StreetNames = ReadStreetNames($"{prefix}.streetnames.txt");
            PostalCodes = ReadPostalCodes($"{prefix}.population_by_postalcode.csv");

            ;
        }
        public static Person Generate()
        {
            return Generate(new PersonOptions());
        }
        public static Person Generate(PersonOptions options)
        {
            var postalCode = GetRandomPostalCode();

            var person = new Person
            {
                LastName = GetRandomValue(LastNames),
                DateOfBirth = GetRandomDoB(options.MinAge ?? 0, options.MaxAge ?? 100),
                StreetAddress = GetRandomStreetAddress(),
                PostalCode = postalCode.PostalCode,
                City = postalCode.City,
                MobileNumber = GetRandomMobileNumber()
            };
            switch (options.Gender ?? (Gender)Random.Next(2))
            {
                case Gender.Female:
                    person.Gender = Gender.Female;
                    person.FirstName = GetRandomValue(FirstNamesWoman);
                    person.MiddleName = GetRandomValue(MiddleNamesWoman);
                    person.SSN = GetSSN(person.DateOfBirth, 2 * Random.Next(50, 499));
                    break;
                case Gender.Male:
                    person.Gender = Gender.Male;
                    person.FirstName = GetRandomValue(FirstNamesMan);
                    person.MiddleName = GetRandomValue(MiddleNamesMan);
                    person.SSN = GetSSN(person.DateOfBirth, 2 * Random.Next(51, 500) - 1);
                    break;
                default:
                    throw new ArgumentException("Invalid value for gender");
            }

            if (options.EmailDomains != null && options.EmailDomains.Length> 0)
            {
                var domain = options.EmailDomains[Random.Next(options.EmailDomains.Length)];
                person.Email = $"{GetEmailUser(person)}@{domain}";
            }

            return person;
        }

        private static List<ValueIndex<string>> ReadNames(string resource)
        {
            var result = new List<ValueIndex<string>>();
            var start = 0;
            using (var stream = typeof(PersonGenerator).Assembly.GetManifestResourceStream(resource))
            using (var reader = new StreamReader(stream))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    var parts = line.Split(';');
                    var count = int.Parse(parts[1]);
                    var end = start + count - 1;

                    result.Add(new ValueIndex<string>
                    {
                        Value = parts[0].Trim(),
                        Start = start,
                        End = end
                    });

                    start += count;
                }
            }

            result.Sort((n1, n2) => n1.Start < n2.Start ?  -1 : 1);
            return result;
        }
        private static List<ValueIndex<string>> ReadStreetNames(string resource)
        {
            var result = new List<ValueIndex<string>>();
            var start = 0;
            using (var stream = typeof(PersonGenerator).Assembly.GetManifestResourceStream(resource))
            using (var reader = new StreamReader(stream))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    result.Add(new ValueIndex<string>
                    {
                        Value = line.Trim(),
                        Start = start,
                        End = start
                    });

                    start++;
                }
            }

            result.Sort((n1, n2) => n1.Start < n2.Start ? -1 : 1);
            return result;
        }
        private static List<ValueIndex<(string PostalCode, string City)>> ReadPostalCodes(string resource)
        {
            var result = new List<ValueIndex<(string, string)>>();
            var start = 0;
            using (var stream = typeof(PersonGenerator).Assembly.GetManifestResourceStream(resource))
            using (var reader = new StreamReader(stream))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    var parts = line.Split(';');
                    var postalCodeParts = parts[0].Split(new[] { '(', ')' });
                    var count = int.Parse(parts[1]);
                    var end = start + count - 1;

                    result.Add(new ValueIndex<(string, string)>
                    {
                        Value = (
                            postalCodeParts[0].Split(' ').First().Trim(),
                            postalCodeParts[1].Trim()
                        ),
                        Start = start,
                        End = end
                    });

                    start += count;
                }
            }

            result.Sort((n1, n2) => n1.Start < n2.Start ? -1 : 1);
            return result;
        }
        
        private static DateTime GetRandomDoB(int minAge, int maxAge)
        {
            var minTime = DateTime.Today.AddYears(-maxAge);
            var maxTime = DateTime.Today.AddDays(1).AddYears(-minAge);
            return minTime.AddHours(Random.Next((int)(maxTime-minTime).TotalHours));
        }
        private static string GetSSN(DateTime dob, int number)
        {
            var separator = dob.Year < 1900 ? '+' : dob.Year < 2000 ? '-' : 'A';
            var checkChar = GetSSNCheckCharacter(dob, number);
            return $"{dob.Day:D2}{dob.Month:D2}{(dob.Year%100):D2}{separator}{number:D3}{checkChar}";
        }
        private static char GetSSNCheckCharacter(DateTime dob, int number)
        {
            var num = int.Parse($"{dob.Day}{dob.Month:D2}{dob.Year % 100}{number}");
            var remainder = num % 31;
            return SSNCheckCharacters[remainder];
        }
        private static T GetRandomValue<T>(List<ValueIndex<T>> index)
        {
            var max = index.Last().End;
            var num = Random.Next(max + 1);

            return index.First(n => n.Start <= num && num <= n.End).Value;
        }
        private static string GetRandomStreetAddress()
        {
            var streetName = GetRandomValue(StreetNames);
            var streetNumber = Random.Next(1, 20);
            var appartmentNumber = Random.Next(1, 30);

            switch (Random.Next(3))
            {
                case 1:
                    return $"{streetName} {streetNumber} as. {appartmentNumber}";
                case 2:
                    var c = (char)('A' + Random.Next(5));
                    return $"{streetName} {streetNumber} {c} {appartmentNumber}";
                default:
                    return $"{streetName} {streetNumber}";
            }
        }
        private static (string PostalCode, string City) GetRandomPostalCode()
        {
            return GetRandomValue(PostalCodes);
        }
        private static string GetEmailUser(Person person)
        {
            string username = person.FirstName;

            var pattern = Random.Next(10);
            if(pattern < 5)
            {
                username += $".{person.LastName}";
            }
            else if(pattern < 8)
            {
                username += $".{person.MiddleName[0]}.{person.LastName}";
            }
            else
            {
                username += (person.DateOfBirth.Year % 100).ToString("D2");
            }

            return new string(username.ToLower()
                .Normalize(System.Text.NormalizationForm.FormD)
                .Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                .ToArray());
        }
        private static string GetRandomMobileNumber()
        {
            var prefix = MobileNumbersPrefixes[Random.Next(MobileNumbersPrefixes.Length)];
            return $"{prefix}{Random.Next(1000000,10000000):D7}";
        }
    }
}
