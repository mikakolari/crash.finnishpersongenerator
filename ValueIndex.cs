﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crash.FinnishPersonGenerator
{
    class ValueIndex<T>
    {
        public T Value { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
    }
}
